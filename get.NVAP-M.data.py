#!/usr/bin/env python
#=========================================================================================
# 	This script downloads NVAP-M data from the ftp.cira.colostate.edu server (i.e. SRC_DIR)
#	to a local directory specified by DST_DIR
#	Make sure to set 'yr' and use m1 and m2 to specify range of months from 1-12
#
#	After running this script, data can be converted from '.mat' to NetCDF using:
#	convert.mat_to_nc.py
#
#	Sept, 2013	Walter Hannah 		University of Miami
#=========================================================================================
import datetime
import sys
import os
import numpy as np
import scipy.io
#=========================================================================================
#=========================================================================================
server 		= ""
#cmd 		= "rsync -uavt "
cmd 		= "wget"
file_type 	= ".mat"

SRC_DIR 	= "ftp://ftp.cira.colostate.edu/ftp/Bytheway/NVAP-M_Climate/Total_column/"
              
DST_DIR 	= "/data2/whannah/OBS/NVAP/raw_nc/"

y1 = 2001
y2 = 2009

#=========================================================================================
#=========================================================================================
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]

for y in range(y1,y2+1):
	yr = "{:0>4d}".format(y)
	os.system(cmd+" "+SRC_DIR+"/"+yr+".*  -P "+DST_DIR)
#=========================================================================================
#=========================================================================================
